
REPO = https://bitbucket.org/cogumbreiro/npb3.0-jav/
ORIG = origin
CHECK = checked

all: compile

run: init
	./benchmarktk/run-benchmark setup.yaml

collect: init
	./benchmarktk/collect-data setup.yaml

pull: update

up: update

data/$(ORIG):
	mkdir -p $@

data/$(CHECK):
	mkdir -p $@

logs/$(ORIG):
	mkdir -p $@

logs/$(CHECK):
	mkdir -p $@

update: init
	git pull
	(cd jarmus; git pull)
	(cd $(CHECK); git pull)
	(cd $(ORIG); git pull)
	(cd stats-tk; git pull)
	(cd benchmarktk; git pull)

compile: init
	(cd jarmus; ant)
	(cp jarmus/target/jarmus.jar $(CHECK)/)
	(cd $(CHECK); make JAVAFLAGS="-cp jarmus.jar:. -O")
	(cd $(ORIG); make JAVAFLAGS="-O")

init: jarmus $(CHECK) $(ORIG) stats-tk benchmarktk \
	data/$(ORIG) data/$(CHECK) logs/$(ORIG) logs/$(CHECK)


jarmus:
	git clone https://bitbucket.org/cogumbreiro/jarmus/

$(CHECK):
	git clone $(REPO) $(CHECK)
	(cd $(CHECK); git checkout jarmus)

$(ORIG):
	git clone $(REPO) $(ORIG)

stats-tk:
	git clone https://bitbucket.org/cogumbreiro/stats-tk/ stats-tk

benchmarktk:
	git clone https://bitbucket.org/cogumbreiro/benchmarktk/

clean:
	(cd jarmus; ant clean)
	(cd $(CHECK); make clean)
	(cd $(ORIG); make clean)
